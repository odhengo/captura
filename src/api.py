import requests
import json

from decouple import config
from flask import jsonify

CHECKER_URL = config('CHECKER_API_URL')

def recognize_on_checker():

    params = { "image": open('./img/opencv_frame.png', 'rb') }

    endpoint = 'http://{}/getPrediction'.format(CHECKER_URL)

    response = requests.post(endpoint, files=params)
    print(response)
    return response.json()