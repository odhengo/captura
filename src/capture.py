import os
import base64

import cv2

from decouple import config

CAM_NUMBER = config('CAM_NUMBER')

def capture_image():
    camera = cv2.VideoCapture(0) 

    _, frame = camera.read()
    
    img_name = "./img/opencv_frame.png"

    if os.path.isfile(img_name):
        os.remove(img_name)        
         
    cv2.imwrite(img_name, frame)

    camera.release()

    return True

def parse_image_b64():
    with open('./img/opencv_frame.png', 'rb') as img_file:
        b64_raw = base64.b64encode(img_file.read())

        b64_encoded = b64_raw.decode('utf-8')

        return b64_encoded